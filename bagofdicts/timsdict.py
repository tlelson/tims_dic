from collections import Iterable

class TimsDict(dict):
    """A better dict

        Allows passing of a list of keys to `values`
    """

    def values(self, keys=None):
        """
        :param keys: Returns all values by default.  If an itterable of keys is provided only the values ascosiated with
        the keys are returned.
        """

        if keys:
            return [self[k] for k in keys]
        else:
            return dict.values(self)
