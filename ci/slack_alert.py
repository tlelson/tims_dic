import requests
import json

## Change:
# 1. webhook url
# 2. channel field
# 3. repository Url

SLACK_CHANNEL = "tttest"
SLACK_WEBHOOK_URL = 'https://hooks.slack.com/services/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'
REPO_URL = 'https://bitbucket.org/tlelson/tims_dic'

# ## Richly formatted

# Get a commit file with the following bash command:
# $ git slack -1 > /tmp/commit.json

commit = {}
with open('/tmp/commit.json', 'r') as f:
    commit = json.loads(f.read())
commit['repository'] = REPO_URL

attachments = [
    {
        "fallback": "The following commit just broke the PyStacks build: <{repository}/commits/{hash}|{hash}>".format(**commit),
        "color": "danger", # Can either be one of 'good', 'warning', 'danger', or any hex color code
        # Fields are displayed in a table on the message
        "fields": [
            {
                "title": "Test failed - PyStack",
                "value": "The following commit just broke the PyStacks build: <{repository}/commits/{hash}|{hash}>".format(**commit),
            }
        ]
    },
    {
        "pretext": "<{repository}/addon/pipelines/home#!|Bitbucket Pipelines>".format(**commit)
    }
]

message_json = {'channel': SLACK_CHANNEL, "username": "BitBucket-Pipelines",
                "text": "<!channel>",
                "link_names": "1",
                "icon_emoji": ":speak_no_evil:",
                "attachments": attachments,
}

response = requests.post(
    url=SLACK_WEBHOOK_URL,
    json=message_json,
)

#return "response.status_code"
