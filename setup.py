from setuptools import setup

def get_version():
    return open('version.txt', 'r').read().strip()

setup(name='bagofdicts',
      version=get_version(),
      description='A bag of assorted Dicts',
      url='https://tlelson@bitbucket.org/tlelson/tims_dic.git',
      author='TLE',
      author_email='tim.l.elson@gmail.com',
      license='MIT',
      packages=['bagofdicts'],
      zip_safe=False)
